#------------------------------------------------------------------------------
# Turnstone Biologics Confidential
# 2018 Turnstone Biologics
# All Rights Reserved.
# 
# This file is subject to the terms and conditions defined in
# file 'license.txt', which is part of this source code package.
#  
# Contributors :
#       Turnstone Biologics - General Release
#-------------------------------------------------------------------------------  
FROM adoptopenjdk/openjdk8

# installing common packages
RUN apt-get update --fix-missing && \
apt-get install -y dos2unix && \
apt-get install -y nano


ARG PROJECT=lambda_demo
ARG PROJECT_DIR=/var/www/${PROJECT}
RUN mkdir -p $PROJECT_DIR
COPY /target/lambda_demo*.jar $PROJECT_DIR/
COPY entrypoint.sh $PROJECT_DIR/
RUN cd $PROJECT_DIR
WORKDIR $PROJECT_DIR

RUN dos2unix /var/www/lambda_demo/entrypoint.sh
RUN chmod +x /var/www/lambda_demo/entrypoint.sh
ENTRYPOINT ["/var/www/lambda_demo/entrypoint.sh"]
