# Specify the provider and access details
provider "aws" {
  region     = "us-east-1"
  access_key = "${var.aws-access-key}"
  secret_key = "${var.aws-secret-key}"
}


# Default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "web" {
#  name        = "lambda_demo-security-group-from-terrorform" #optional, when omitted, terraform creates a random name
  description = "security group for lambda_demo web created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Our default security group to for the database
resource "aws_security_group" "rds" {
  description = "security group for lambda_demo RDS created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # mysql access from anywhere
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "default" {
  depends_on             = ["aws_security_group.rds"]
#  identifier             = "lambda_demo-rds" # Terraform will create a unique id if not assigned
  allocated_storage      = "10"
  engine                 = "mysql"
  engine_version         = "8.0.15"
  instance_class         = "db.t2.micro"
  name                   = "lambda_demo"
  username               = "root"
  password               = "6969Cutlass!!"
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
}

resource "aws_instance" "web" {
  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
 }

  instance_type = "t2.micro"
  
  tags { Name = "lambda_demo instance" } 

  # standard realmethods community AMI with docker pre-installed
  ami = "ami-05033408e5e831fb0"

  # The name of the  SSH keypair you've created and downloaded
  # from the AWS console.
  #
  # https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#KeyPairs:
  #
  key_name = "my-public-key"
  
  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.web.id}"]

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo docker login --username tylertravismya --password 69cutlass",
      "sudo docker pull realmethods/lambdademo:latest",
      "sudo docker run -it -d -p 8000:8000 -p 8080:8080 -e DATABASE_URL=jdbc:mysql://${aws_db_instance.default.endpoint}/lambda_demo realmethods/lambdademo:latest"
    ]
  }
}
resource "aws_lambda_function" "getAlleyLeagues" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAlleyLeagues"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::getLeagues"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addAlleyLeagues" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "addAlleyLeagues"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::addLeagues"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignAlleyLeagues" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "assignAlleyLeagues"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::assignLeagues"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAlleyLeagues" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteAlleyLeagues"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::deleteLeagues"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAlleyTournaments" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAlleyTournaments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::getTournaments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addAlleyTournaments" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "addAlleyTournaments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::addTournaments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignAlleyTournaments" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "assignAlleyTournaments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::assignTournaments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAlleyTournaments" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteAlleyTournaments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::deleteTournaments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAlleyLanes" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAlleyLanes"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::getLanes"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addAlleyLanes" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "addAlleyLanes"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::addLanes"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignAlleyLanes" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "assignAlleyLanes"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::assignLanes"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAlleyLanes" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteAlleyLanes"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::deleteLanes"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createAlley" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "createAlley"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::createAlley"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAlley" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAlley"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::getAlley"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllAlley" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAllAlley"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::getAllAlley"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAlley" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveAlley"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::saveAlley"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAlley" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteAlley"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.AlleyAWSLambdaDelegate::deleteAlley"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getGameMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getGameMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::getMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveGameMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveGameMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::saveMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteGameMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteGameMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::deleteMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getGamePlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getGamePlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::getPlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveGamePlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveGamePlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::savePlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteGamePlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteGamePlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::deletePlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createGame" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "createGame"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::createGame"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getGame" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getGame"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::getGame"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllGame" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAllGame"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::getAllGame"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveGame" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveGame"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::saveGame"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteGame" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteGame"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.GameAWSLambdaDelegate::deleteGame"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createLane" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "createLane"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LaneAWSLambdaDelegate::createLane"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getLane" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getLane"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LaneAWSLambdaDelegate::getLane"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllLane" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAllLane"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LaneAWSLambdaDelegate::getAllLane"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveLane" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveLane"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LaneAWSLambdaDelegate::saveLane"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteLane" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteLane"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LaneAWSLambdaDelegate::deleteLane"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getLeaguePlayers" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getLeaguePlayers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::getPlayers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addLeaguePlayers" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "addLeaguePlayers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::addPlayers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignLeaguePlayers" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "assignLeaguePlayers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::assignPlayers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteLeaguePlayers" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteLeaguePlayers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::deletePlayers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createLeague" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "createLeague"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::createLeague"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getLeague" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getLeague"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::getLeague"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllLeague" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAllLeague"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::getAllLeague"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveLeague" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveLeague"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::saveLeague"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteLeague" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteLeague"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.LeagueAWSLambdaDelegate::deleteLeague"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getMatchupGames" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getMatchupGames"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::getGames"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addMatchupGames" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "addMatchupGames"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::addGames"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignMatchupGames" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "assignMatchupGames"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::assignGames"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteMatchupGames" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteMatchupGames"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::deleteGames"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "createMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::createMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::getMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAllMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::getAllMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::saveMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteMatchup" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteMatchup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.MatchupAWSLambdaDelegate::deleteMatchup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createPlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "createPlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.PlayerAWSLambdaDelegate::createPlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getPlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getPlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.PlayerAWSLambdaDelegate::getPlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllPlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAllPlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.PlayerAWSLambdaDelegate::getAllPlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "savePlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "savePlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.PlayerAWSLambdaDelegate::savePlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deletePlayer" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deletePlayer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.PlayerAWSLambdaDelegate::deletePlayer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getTournamentMatchups" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getTournamentMatchups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::getMatchups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addTournamentMatchups" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "addTournamentMatchups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::addMatchups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignTournamentMatchups" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "assignTournamentMatchups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::assignMatchups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteTournamentMatchups" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteTournamentMatchups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::deleteMatchups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createTournament" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "createTournament"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::createTournament"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getTournament" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getTournament"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::getTournament"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllTournament" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "getAllTournament"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::getAllTournament"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveTournament" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "saveTournament"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::saveTournament"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteTournament" {
  filename         = "/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar"
  function_name    = "deleteTournament"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.occulue.delegate.TournamentAWSLambdaDelegate::deleteTournament"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambda_demo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      delegateDAOHost = "${aws_instance.web.public_ip}"
      delegateDAOPort = "8080"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}




